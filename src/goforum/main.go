package main

import (
	"fmt"
	"log"
	"net/http"
)

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "You have visited %s!", r.URL.Path)
}

func main() {

	http.HandleFunc("/", handle)
	fmt.Println("Starting web server on port 8080")
	httpdErr := http.ListenAndServe(":8080", nil)
	if httpdErr != nil {
		log.Fatal(httpdErr)
	}
}
